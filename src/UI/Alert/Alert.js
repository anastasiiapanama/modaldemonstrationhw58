import React from 'react';

const Alert = props => {
    return (
        <div className={["Alert", props.type].join(' ')}
             style={{
                 transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                 opacity: props.show ? '1' : '0'
             }}
        >
            {props.children}
            {props.dismiss && <button className="ButtonAlert" onClick={props.dismiss}>X</button>}
        </div>
    );
};

export default Alert;