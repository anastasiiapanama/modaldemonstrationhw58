import React, {useState} from 'react';
import './App.css';
import Alert from "./UI/Alert/Alert";

const App = () => {
    const [modalHide, setModalHide] = useState(false);
    const [alertHide, setAlertHide] = useState(false);

    const Modal = props => {
        return (
            <>
                <Backdrop show={props.show} onClick={props.closed}/>
                <div
                    className="Modal"
                    style={{
                        transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                        opacity: props.show ? '1' : '0'
                    }}
                >
                    <div className="ModalItem">
                        {props.title}
                        <Button onClick={modalCancelHandler}>X</Button>
                    </div>
                    {props.children}
                </div>
            </>
        );
    };

    const Backdrop = ({show, onClick}) => (
        show ? <div className="Backdrop" onClick={onClick}/> : null
    );

    const Button = ({onClick, children}) => (
        <button
            className="Button"
            onClick={onClick}
        >
            {children}
        </button>
    );

    const modalHandler = () => {
        setModalHide(true);
    };

    const modalCancelHandler = () => {
        setModalHide(false);
    };

    const alertHandler = () => {
        setAlertHide(true);
    };

    const alertCancelHandler = () => {
        setAlertHide(false);
    };

    return (
        <>
            <div className="button-container">
                <button
                    className="ModalButton"
                    onClick={modalHandler}
                >Click Me
                </button>

                <button
                    className="AlertButton"
                    onClick={alertHandler}
                >Alert Button
                </button>
            </div>

            <Modal
                show={modalHide}
                closed={modalCancelHandler}
                title="Some kinda modal title"
            >
                <p>This is modal content</p>
            </Modal>

            <Alert
                type="warning"
                dismiss={alertCancelHandler}
                show={alertHide}
            >
                This is warning type alert
            </Alert>

            <Alert
                type="success"
                show={alertHide}
            >
                This is a success type alert
            </Alert>
        </>
    );
};

export default App;
